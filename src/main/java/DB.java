import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Database connection using singleton pattern to ensure only one connection to the database
 * To get the connection call DB.getInstance().getConnection()
 */
public class DB {

    private static DB instance;
    private Connection connection;

    private DB() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Properties p = new Properties();
            p.put("user", "root");
            this.connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1/tdt4145?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false", p);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DB getInstance() throws SQLException {
        if (instance == null) {
            instance = new DB();
        } else if (instance.getConnection().isClosed()) {
            instance = new DB();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }


}
