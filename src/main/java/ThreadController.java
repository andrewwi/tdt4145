import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Creating new treads in database
 */
public class ThreadController {

    public static Thread createThread(Folder folder) throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select count(*) from thread";
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        int threadCount = rs.getInt(1);
        Thread thread = new Thread(folder, ++threadCount);
        query = "insert into thread value('" + thread.getThreadId() + "','" +
                thread.getFolder().getFolder_name() + "','" +
                thread.getFolder().getForum().getCourse_name() + "','" +
                thread.getFolder().getForum().getTerm() +
                "');";
        stmt.executeUpdate(query);
        return thread;
    }
}
