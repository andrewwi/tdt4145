import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * FolderController for handeling SQL operations with folders.
 */
public class FolderController {
    // Get all the folders as a list
    public static ArrayList<Folder> getAllFolders(Forum forum) throws SQLException {
        ArrayList<Folder> folders = new ArrayList<Folder>();

        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select folder_name from folder where course_name='" + forum.getCourse_name() + "' and term='" + forum.getTerm() + "';";
        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            folders.add(new Folder(rs.getString("folder_name"), forum));
        }

        return folders;
    }

    // Create a new folder
    public static Folder createFolder(String folder_name, Forum forum) throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "insert into folder value('" + folder_name + "','" + forum.getCourse_name() + "','" + forum.getTerm() + "');";
        stmt.executeUpdate(query);
        return new Folder(folder_name, forum);
    }
}
