import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Read and write user informasjon from DB
 */
public class UserController {


    public static ArrayList<User> getAllUsers() throws SQLException {
        ArrayList users = new ArrayList<User>();

        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select email, password, role from user";
        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            users.add(new User(rs.getString("email"), rs.getString("password"), rs.getString(("role"))));
        }

        return users;
    }

    // Get user from DB. By passing email and password this is used as autentication
    public static User getUser(String email, String password) throws SQLException {
        User user = null;
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select email, password, role from user where email='" + email + "'and password='" + password + "'";
        ResultSet rs = stmt.executeQuery(query);

        // If we have a match, set this match to the user
        if (rs.next()) {
            user = new User(rs.getString("email"), rs.getString("password"), rs.getString(("role")));
        }
        return user;
    }
}
