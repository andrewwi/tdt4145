/**
 * Class for modeling the forum entity
 */
public class Forum {
    private String course_name;
    private String term;

    public Forum(String course_name, String term) {
        this.course_name = course_name;
        this.term = term;
    }

    public String getCourse_name() {
        return course_name;
    }

    public String getTerm() {
        return term;
    }
}
