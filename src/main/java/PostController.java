import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Reading and writing posts to the DB
 */
public class PostController {
    public static ArrayList<Post> getAllPosts() throws SQLException {
        // Get all posts sorted by threadID and then postID
        ArrayList posts = new ArrayList<Post>();


        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select * from post order by threadid, postid";
        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            posts.add(new Post(rs.getInt("postid"), rs.getInt("threadid"), rs.getString(3), rs.getString(4)));
        }

        return posts;
    }

    public static Post getPostByID(int id) throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select * from post where postid='" + id + "';";
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        return new Post(rs.getInt("postid"), rs.getInt("threadid"), rs.getString(3), rs.getString(4));
    }

    public static Post createPost(Thread thread, User user, String text) throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        Post post = new Post(thread, user, text);
        String query = "insert into post value('" + post.getPostId() + "','" +
                post.getThreadId() + "','" +
                post.getEmail() + "','" +
                text +
                "');";
        stmt.executeUpdate(query);
        return post;

    }

    public static Post createPost(int threadid, String email, String text) throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        Post post = new Post(threadid, email, text);
        String query = "insert into post value('" + post.getPostId() + "','" +
                post.getThreadId() + "','" +
                post.getEmail() + "','" +
                text +
                "');";
        stmt.executeUpdate(query);
        return post;
    }

}
