import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws SQLException, IOException {
        User currentUser = null;
        Forum currentForum = new Forum("TDT4145", "Spring21");

        Thread currentThread;
        Folder currentFolder;

        int menuChoice = 0;
        int tmploop = 10;
        Scanner scanner = new Scanner(System.in);

        // Use while loop to print menu in the terminal
        while (menuChoice >= 0 && tmploop > 0) {
            tmploop--;

            // If we have a user, we are logged in
            if (currentUser != null) {
                System.out.println("");
                System.out.println("Logged in as");
                System.out.println(currentUser);
                System.out.println("1) Change user");
                System.out.println("2) Create new post");
                System.out.println("3) Reply to thread");
                System.out.println("4) Search posts");
                System.out.println("5) Read post");
                System.out.println("6) Show statistics");
                menuChoice = scanner.nextInt();
                scanner.nextLine(); // need to "consume" newline from scanner.nextint()
                if (menuChoice == 1) {
                    // Change user
                    System.out.println("Logged out");
                    currentUser = null;
                } else if (menuChoice == 2) {
                    // Create new post
                    System.out.println("Enter text for the post:");
                    String postText = scanner.nextLine();

                    // Select folder
                    System.out.println("Select folder:");
                    int i = 0;
                    System.out.println("0) Create new folder");

                    // Loop all folders in forum and print them with a number to indicate what to choose
                    for (Folder folder : FolderController.getAllFolders(currentForum)) {
                        System.out.print(++i);
                        System.out.print(") ");
                        System.out.println(folder);
                    }
                    // Get user choice
                    int folderChoice = scanner.nextInt();
                    scanner.nextLine(); // need to "consume" newline from scanner.nextint()
                    // If users select 0, we create new folder
                    if (folderChoice == 0) {
                        System.out.println("Enter folder name:");

                        String folderName = scanner.nextLine();
                        currentFolder = FolderController.createFolder(folderName, currentForum);

                    } else {
                        // Select folder from user input
                        currentFolder = FolderController.getAllFolders(currentForum).get(folderChoice - 1);
                    }

                    // Select tag
                    System.out.println("Select tag:");
                    i = 0;
                    Tag currentTag;
                    System.out.println("0) Create new tag");
                    // Loop all folders in forum and print them with a number to indicate what to choose
                    for (Tag tag : TagController.getAllTags()) {
                        System.out.print(++i);
                        System.out.print(") ");
                        System.out.println(tag);
                    }
                    // Get user choice
                    int tagChoice = scanner.nextInt();
                    scanner.nextLine(); // need to "consume" newline from scanner.nextint()
                    // If users select 0, we create new folder
                    if (tagChoice == 0) {
                        System.out.println("Enter tag name:");

                        String tagName = scanner.nextLine();
                        currentTag = TagController.createTag(tagName);


                    } else {
                        // Select folder from user input
                        currentTag = TagController.getAllTags().get(tagChoice - 1);
                    }
                    Thread currentTread = ThreadController.createThread(currentFolder);
                    System.out.println("Tag created");
                    Post newPost = PostController.createPost(currentTread, currentUser, postText);
                    System.out.println("new post created");
                    // Link tag to post
                    TagController.addTag(currentTag, newPost.getPostId());
                    System.out.println("tagged post ok");

                } else if (menuChoice == 3) {
                    // Reply to post

                    int currentThreadID = -1;
                    for (Post post : PostController.getAllPosts()) {
                        if (post.getThreadId() != currentThreadID) {
                            currentThreadID = post.getThreadId();

                            System.out.println("Thread #" + post.getThreadId());
                            System.out.println("--------------");
                        }
                        System.out.println(post);
                    }
                    System.out.println("Select thread to reply to: ");
                    int threadChoice = scanner.nextInt();
                    scanner.nextLine(); // consume nextInt() newline
                    System.out.println("Enter text:");
                    String postText = scanner.nextLine();
                    PostController.createPost(threadChoice, currentUser.getEmail(), postText);

                } else if (menuChoice == 4) {
                    System.out.println("Enter text to search for:");
                    String searchText = scanner.nextLine();
                    Statement stmt = DB.getInstance().getConnection().createStatement();
                    String query = "select postid from post where text like '%" + searchText + "%';";
                    ResultSet rs = stmt.executeQuery(query);
                    System.out.println("PostIds with matching search:");
                    while (rs.next()) {
                        System.out.println(rs.getInt(1));
                    }

                } else if (menuChoice == 5) {
                    for (Post post : PostController.getAllPosts()) {
                        System.out.println(post.getPostId());
                    }

                    System.out.println("Select post to read: ");
                    int currentPostID = scanner.nextInt();
                    scanner.nextLine(); //consume nextInt() newline
                    // find the post from the db
                    System.out.println(PostController.getPostByID(currentPostID));
                    // mark it as read
                    Statement stmt = DB.getInstance().getConnection().createStatement();
                    String query = "insert into viewed value('" + currentUser.getEmail() + "'," + currentPostID + ",default)";
                    stmt.executeUpdate(query);

                } else if (menuChoice == 6) {
                    Statement stmt = DB.getInstance().getConnection().createStatement();
                    String query = "select user.email as username, count(DISTINCT viewed_at) as postsRead, count(DISTINCT post.postID) as postsCreated\n" +
                            "from user \n" +
                            "LEFT OUTER JOIN viewed ON user.email = viewed.email \n" +
                            "LEFT OUTER JOIN post on user.email = post.email \n" +
                            "group by user.email\n" +
                            "order by postsRead desc, postsCreated desc;\n";
                    ResultSet rs = stmt.executeQuery(query);
                    System.out.println("Username - # post read - # posts created");
                    while (rs.next()) {
                        System.out.println(rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3));
                    }


                } else {
                    System.out.println("Invalid menu option. Exiting.");
                    menuChoice = -1;
                }
                // No user = not logged in
            } else {
                System.out.println("");
                System.out.println("1) Show users");
                System.out.println("2) Login");
                menuChoice = scanner.nextInt();
                scanner.nextLine(); // need to "consume" newline from scanner.nextint()

                if (menuChoice == 1) {
                    // Print all users including password to make it easier to use the program
                    for (User user : UserController.getAllUsers()) {
                        System.out.println(user);
                    }
                } else if (menuChoice == 2) {
                    System.out.println("Email: ");
                    String email = scanner.next();
                    System.out.println("Password: ");
                    String password = scanner.next();
                    currentUser = UserController.getUser(email, password);
                    if (currentUser == null) {
                        System.out.println("Invalid email/password");
                    }
                } else {
                    System.out.println("Invalid menu option. Exiting.");
                    menuChoice = -1;
                }
            }
        }


    }
}
