/**
 * Folder class for modeling the folder
 */
public class Folder {
    private String folder_name;
    private Forum forum;

    public Folder(String folder_name, Forum forum) {
        this.folder_name = folder_name;
        this.forum = forum;
    }

    public String getFolder_name() {
        return folder_name;
    }

    public Forum getForum() {
        return forum;
    }

    @Override
    public String toString() {
        return folder_name;
    }
}
