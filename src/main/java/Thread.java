public class Thread {
    private int threadId;
    private Folder folder;

    public Thread(Folder folder, int threadId) {
        this.threadId = threadId;
        this.folder = folder;
    }

    @Override
    public String toString() {
        return "Thread{" +
                "threadId=" + threadId +
                ", folder_name='" + folder.getFolder_name() + '\'' +
                ", forum=" + folder.getForum().getCourse_name() + " " + folder.getForum().getTerm() +
                '}';
    }

    public int getThreadId() {
        return threadId;
    }

    public Folder getFolder() {
        return folder;
    }
}
