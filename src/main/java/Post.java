import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class for a single post
 */
public class Post {

    private int postId;
    private int threadId;
    private String email;
    private String text;

    public Post(Thread thread, User user, String text) throws SQLException {
        // USed for creating new posts
        setPostId();
        this.threadId = thread.getThreadId();
        this.email = user.getEmail();
        this.text = text;

    }

    public Post(int threadid, String email, String text) throws SQLException {
        // USed for creating new posts
        setPostId();
        this.threadId = threadid;
        this.email = email;
        this.text = text;
    }

    public Post(int postId, int threadid, String email, String text) throws SQLException {
        // Used when reading data from DB
        this.postId = postId;
        this.threadId = threadid;
        this.email = email;
        this.text = text;
    }

    // Internal used to set the correct postID. Need to read this for DB to have the correct one
    private void setPostId() throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select count(*) from post";
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        this.postId = rs.getInt(1) + 1;
    }


    public int getPostId() {
        return postId;
    }

    public String getText() {
        return text;
    }

    public int getThreadId() {
        return threadId;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Post{" +
                "postId=" + postId +
                ", threadId=" + threadId +
                ", email='" + email + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
