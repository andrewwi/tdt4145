import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Reading and writing tags to DB
 */
public class TagController {

    public static ArrayList<Tag> getAllTags() throws SQLException {
        ArrayList tags = new ArrayList<Tag>();

        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "select tag_name from tag;";
        ResultSet rs = stmt.executeQuery(query);

        while (rs.next()) {
            tags.add(new Tag(rs.getString(1)));
        }

        return tags;
    }

    public static Tag createTag(String tag) throws SQLException {
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "insert into tag value('" + tag + "');";
        stmt.executeUpdate(query);
        return new Tag(tag);
    }

    public static void addTag(Tag tag, int postid) throws SQLException {
        // Add a tag to a post
        Statement stmt = DB.getInstance().getConnection().createStatement();
        String query = "insert into taggedwith value('" + tag.getTag() + "','" + postid + "');";
        stmt.executeUpdate(query);
    }
}
