-- TDT4145 Prosjekt 1
-- Gruppe 172
-- Medlemmer: Andreas Wågø Wilson
DROP DATABASE tdt4145;
CREATE DATABASE tdt4145;
USE tdt4145;
CREATE TABLE user
(
    email    varchar(100),
    password varchar(100),
    ROLE     varchar(100),
    CONSTRAINT user_pk PRIMARY KEY (email)
);
CREATE TABLE forum
(
    course_name varchar(100),
    term        varchar(100),
    CONSTRAINT forum_pk PRIMARY key (course_name, term)
);
CREATE TABLE memberof
(
    email       varchar(100),
    course_name varchar(100),
    term        varchar(100),
    CONSTRAINT memberof_pk PRIMARY key (email, course_name, term),
    CONSTRAINT memberof_fk_user
        FOREIGN KEY (email) REFERENCES user (email) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT memberof_fk_forum
        FOREIGN KEY (course_name,
                     term) REFERENCES forum (course_name, term) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE folder
(
    folder_name varchar(100),
    course_name varchar(100),
    term        varchar(100),
    CONSTRAINT folder_pk PRIMARY key (folder_name),
    CONSTRAINT forum_folder_fk_forum
        FOREIGN key (course_name, term) REFERENCES forum (course_name, term) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE parentfold
(
    parent_folder varchar(100),
    course_name   varchar(100),
    child_folder  varchar(100),
    CONSTRAINT parent_folder_pk PRIMARY key (parent_folder, course_name, child_folder),
    CONSTRAINT parent_folder_fk_parent
        FOREIGN key (parent_folder) REFERENCES folder (folder_name) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT parent_folder_fk_forum
        FOREIGN key (course_name) REFERENCES forum (course_name) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT parent_folder_fk_child
        FOREIGN key (child_folder) REFERENCES folder (folder_name) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE thread
(
    threadid    INT AUTO_INCREMENT,
    folder_name varchar(100),
    course_name varchar(100),
    term        varchar(100),
    CONSTRAINT thread_pk PRIMARY key (threadid),
    CONSTRAINT thread_fk_forum
        FOREIGN key (course_name, term) REFERENCES forum (course_name, term) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT thread_fk_folder
        FOREIGN key (folder_name) REFERENCES folder (folder_name) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE post
(
    postid   INT AUTO_INCREMENT,
    threadid int,
    email    varchar(100),
    text     TEXT,
    CONSTRAINT post_pk PRIMARY key (postid),
    CONSTRAINT post_fk_thread
        FOREIGN key (threadid) REFERENCES thread (threadid) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT post_fk_user
        FOREIGN key (email) REFERENCES user (email)
);
CREATE TABLE goodcomment
(
    email  varchar(100),
    postid INT,
    CONSTRAINT good_comment_pk PRIMARY key (email, postid),
    CONSTRAINT good_comment_fk_user
        FOREIGN key (email) REFERENCES user (email) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT good_comment_fk_post
        FOREIGN key (postid) REFERENCES post (postid) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE tag
(
    tag_name varchar(100),
    CONSTRAINT tag_pk PRIMARY KEY (tag_name)
);
CREATE TABLE taggedwith
(
    tag_name varchar(100),
    postid   INT,
    CONSTRAINT tagged_with_pk PRIMARY key (tag_name, postid),
    CONSTRAINT tagged_with_fk_tag
        FOREIGN key (tag_name) REFERENCES tag (tag_name) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT tagged_with_fk_post
        FOREIGN key (postid) REFERENCES post (postid) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE viewed
(
    email     varchar(100),
    postid    INT,
    viewed_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT viewed_pk PRIMARY key (email, postid, viewed_at),
    CONSTRAINT viewed_fk_user
        FOREIGN key (email) REFERENCES user (email) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT viewed_fk_post
        FOREIGN key (postid) REFERENCES post (postid) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO forum VALUE ('TDT4145', 'Spring21');
INSERT INTO user
VALUES ('student@ntnu.no', 'pass', 'Student'),
       ('instructor@ntnu.no', 'pass', 'Instructor');
INSERT INTO memberof
VALUES ('student@ntnu.no', 'TDT4145', 'Spring21'),
       ('instructor@ntnu.no', 'TDT4145', 'Spring21');
INSERT INTO folder VALUE ('Exam', 'TDT4145', 'Spring21');
INSERT INTO tag VALUE ('Question');

